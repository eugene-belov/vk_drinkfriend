package sys.constants
{
	public class GameGlobals
	{
		
		private static var __instance:GameGlobals = new GameGlobals();
		public function GameGlobals()
		{
			super();
			if (__instance != null)
			{
				throw new Error("Singleton can only be accessed through ItemsManager.instance");
			}
		}
		public static function get instance():GameGlobals {	return __instance; }
		
		private var _vkUserId					:Number = 0;
		private var _vkAuthKey					:String = "";
		private var _vkSecret					:String;
		private var _ballCosts					:Number = 0.02; // Kurs valuty
		private var _urlGateway					:String = "http://aagq278a.facebook.joyent.us/lines/vk/gateway.php";//"http://workunion-games.ru/vk/gateway.php";
		private var _urlXmlVotes				:String = "http://workunion-games.ru/vk/votes.xml";//"http://aagq278a.facebook.joyent.us/lines/flash_1/assets/votes.xml";

		
		public function get vkSecret():String 			{ return _vkSecret; }
		public function set vkSecret(value:String):void { _vkSecret = value; }
		
		public function get vkUserId():Number 			{ return _vkUserId; }
		public function set vkUserId(value:Number):void	{ _vkUserId = value; }

		public function get vkAuthKey():String 			 { return _vkAuthKey; }
		public function set vkAuthKey(value:String):void { _vkAuthKey = value; }

		public function get ballCosts():Number 			{ return _ballCosts; }
		public function set ballCosts(value:Number):void { _ballCosts = value;}

		public function get urlGateway():String	{ return _urlGateway; }
		public function set urlGateway(value:String):void { _urlGateway = value; }

		public function get urlXmlVotes():String { return _urlXmlVotes; }
		public function set urlXmlVotes(value:String):void { _urlXmlVotes = value; }


	}
}